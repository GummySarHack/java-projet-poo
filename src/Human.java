
public class Human 
{
	String name;
	int age;
	int heightInCentimetres;
	String eyeColour;

	/**
	 *Constructor 
	 */
	public Human() 
	{
		
	}
	
	public void Speak()
	{
		System.out.println("Greetings! My name is " + name );
		System.out.println("I am " + age);
		System. out.println("I am " + heightInCentimetres + " centimetres tall!");
		System.out.println("My eye colour is " + eyeColour);
	}
	
	//How human eats
	public void Eat()
	{
		System.out.println("Eating...");
	}
	
	public void Walk()
	{
		System.out.println("Walk...");
	}
	
	public void Work()
	{
		System.out.println("Work...");
	}

}
